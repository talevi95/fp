@extends('layouts.sidebar')

@section('title','Orders')

@section('content')
       <h1>Create Order</h1>
        <form method='post' action="{{action('OrdersController@store')}}">
        @csrf 

        <!-- <div class="form-group">
            <label for = "customer">customer</label>
            <input type = "text" class="form-control" name = "customer">
        </div> -->
        <div class="form-group">
            <label for = "product">Product</label>
            <select class="form-control" id="selectProduct" name="product_selected" required focus>
            
    @foreach($products as $product)
    <option value="{{$product->id}}">{{ $product->name }}</option>
    @endforeach
  </select>
           
        </div>
        <div class="form-group">
            <label for = "model">Engine Code</label>
            <input type = "text" class="form-control" name = "model" required >
        </div>
        <div class="form-group">
            <label for = "customer">Customer</label>
            <input type = "text" class="form-control" name = "customer" required >
        </div>

        <div class="form-group">
            <label for = "address">Address</label>
            <input type = "text" class="form-control" name = "address" required >
        </div>
        <div class="form-group">
            <label for = "totalPrice">Price Without TAX</label>
            <input type = "number" class="form-control" name = "totalPrice" required >
        </div>
        <div class="form-group">
            <label for = "isPaid">Paid?</label>
            <input type = "text" class="form-control" name = "isPaid" required >
        </div>
        <div class="form-group">
            <label for = "telephone">Telephone</label>
            <input type = "number" class="form-control" name = "telephone" required >
        </div>
        <div class="form-group">
            <label for = "date">Delivery Date</label>
            <input type="date" name="deliveryDate" id="date" class="form-control" value="{{ date("d-m-y") }}" required >
        </div>
        <div class="form-group">
            <label for = "remarks">Remarks</label>
            <input type = "text" class="form-control" name = "remarks" >
        </div>
        <div class="form-group">
            <label for = "userid">User</label>
            <select class="form-control" id="selectUser" name="user_selected" required focus>
            
    @foreach($users as $user)
    <option value="{{$user->id}}" @if ($user->id == $user_name->id) selected @endif>{{ $user->name }}</option>
    @endforeach
  </select>
          
        </div> 


         <div class="form-group">
            <input type="submit" class="btn btn-success btn-block" name="submit" value="Save">
        </div>                     
        </form>  


@endsection
